# Live Video Server für den experimentellen mur.at kubernetes cluster

Der hier vorliegende Live Video Streaming Server basiert auf Vorarbeiten, die
ursprünglich für das Forum Stadtpark entwickelt wurden. Das Setup wurde ein
wenig bereinigt, um es automatisch per GitLab CI system in ein fertiges
Docker-Image zu übersetzten. Da mittlerweile auch das benötigte
[nginx-RTMP-modul](https://packages.debian.org/testing/libnginx-mod-rtmp) für
die adaptive Videoauslieferung unter debian testing fertig zur Verfügung steht,
gestaltet sich das aber ohnedies relativ einfach und übersichtlich.

## Allgemeines zur Benutzung des Docker Images

Obwohl in der Regel ohnehin nur ganz geringe Anpassungen für die praktische
Verwendung in abweichenden Anwendungsfällen vorzunehmen sind, orientiert sich
die hiesige Umsetzung streng am Vorbild der *"immutable containers"*. Änderungen
für anders gelagerte Anwedungsfälle -- also bspw., wenn eine Aufzeichnung des
Streams am Server notwendig ist -- sollten an einem geklonten Repository
vorgenommen werden und in einen eigenen neuen Anwedungscontainer gepackt werden.
Für sehr viele Anwendungsfälle dürften die Voreinstellungen in diesem Repository
bzw. der fertig vorfügbarem Container aber völlig ausreichen.

Es können damit in der vorliegenden Form auch mehrere Übertragungen parallel
abgewickelt werden und individuelle URLs für die einzelnen Streams frei gewählt werden.

Als Muster für die Form der URLs gilt dabei:

für den Upstream: `rtmp://servername/live/stream-name`  
für die Web-Page: `http://servername#stream-name`

![Browser](images/browser.png)

## Benutzung auf einfacher Docker-Infrastruktur

Das Verzeichnise mit den den temporären HLS Video-Fragmenten sollte nach
möglichkeit über eine explizit Volume-Spezifikation als `tmpfs` eingebunden
werden, um effizientere Schreibzugriffe zu gewährleisten als es die
Docker-üblichen Overlay-Filesysteme erlauben. 

Starten des Servers auf Docker Infrastruktur:

    docker run -d --restart unless-stopped \
	-p 80:80 -p 1935:1935 \
	--mount 'type=tmpfs,dst=/tmp/hls' \
	--name live-video \
	registry.gitlab.com/mur-at-public/live-video

## Updaten des Docker Images auf Docker-Infrastruktur

Wenn im Git-Repository Änderung vorgenommen wurden und auf GitLab ein
neues Docker Image generiert wurde, muss dieses manuell aktualisiert werden.
Die dafür notwendige Befehlssequenz lautet:

	docker stop live-video
	docker rm live-video
    docker pull registry.gitlab.com/mur-at-public/live-video
	docker run ... [wie oben]

## Senden des Live Videos

Der Upstream der Live Übertragung benutzt das RTMP-Übertragungsrotokoll und
kann mit verschiedensten Lösungen realisiert werden.

### Übertragung mittels Handy

Am einfachsten geht es vermutlich mit dem Handy. Zum Beispiel mit dem
kostenlosen ["Larix Broadcaster"](https://softvelum.com/larix/), den es sowohl
[für
Android](https://play.google.com/store/apps/details?id=com.wmspanel.larix_broadcaster)
als auch [für
iPhones](https://apps.apple.com/us/app/larix-broadcaster/id1042474385) gibt.

Bei den Einstellungen zum Einrichten einer neuen Verbindung muss hier eigentlich
nur URL korekt eingetragen werden:

![Larix Broadcaster Settings](images/Screenshot_Larix_Broadcaster.png)

### OBS am Computer

Eine weitere komfortable Möglichkeit bildet das Programm ["OBS
Studio"](https://obsproject.com/), das auf Windows, Macintosh und Linux Rechner
läuft. Mit angeschlossener Webcam od. über Video-IO Karten kann man hier die
Übertragung sehr gut kontrollieren und auch gestalterisch (Titeleinblendung
etc.) eingreifen.

![OBS Main Screen](images/obs-main-screenshot.png)

Abgesehen von der Einrichtung der Eingabegeräte, sind hier die
Verbindungseinstellunge (der Upload-URL verteilt sich hier auf zwei Felder!) und
Video-Settings vorzunehmen:

![OBS Connection Settings](images/obs-connection-settings.png)
![OBS Video Settings](images/obs-video-settings.png)

### Von der Kommandozeile

Man kann aber natürlich auch das command line tool `ffmpeg`, das im Hintergrund
in vielen dieser Werkzeugen die tatsächliche Arbeit abwickelt, auch ohne
grafische Hilfsoberfläche zum Streamen nutzen. So kann man dann auch bspw. einen
Raspberry Pi für diesen Zweck nutzen. Im Falle einer Webcam, die man mittels
V4L2 anspricht, sieht das etwa so aus:

    #!/bin/sh

    ffmpeg \
	-f alsa  -ac 1 -i hw:U0x46d0x81d \
	-f v4l2 -framerate 24 -input_format yuyv422 \
	-video_size 752x416 -i /dev/video0 \
	-hide_banner -af aresample=async=1000 -pix_fmt yuv420p -ac 2 \
	-c:v libx264 -preset faster -b:v 2M \
	-c:a aac -b:a 128k -bufsize 256 \
	-f flv rtmp://servername.mur.at/live/stream-name

Am besten schreibt man diesen Befehl in einen shell-script (bspw.
`/home/machine/rtmp-encoder.sh`) und erlaubt dessen Ausführung.

(In manchen Anwendungsszenarien, die über LTE-Router abgewickelt werde, ist
zusätzlich noch ein Netzerk-Tunnel bzw. lokales port-forwarding nötig, um eine
funktionierende Verbindung zu erzielen: `ssh -f -L 1935:servername.mur.at:1935
user@servername.mur.at -N`)

Um das script bei jedem Neustart via Systemd auszuführen, benötigt man
darüber hinaus auch noch ein Service-File folgender Form
`/lib/systemd/system/rtmp-encoder.service`:

```
[Unit]
Description=RTMP LiveStreaming Encoder
ConditionFileIsExecutable=/home/machine/rtmp-encoder.sh
After=network.eth0

[Service]
ExecStart=/home/machine/rtmp-encoder.sh
Restart=always
RestartSec=15

[Install]
WantedBy=multi-user.target
```

Nun muss der Service noch enabled und gestartet werden:

    sudo systemctl enable rtmp-encoder
    sudo systemctl start rtmp-encoder

## Ausmessen der verfügbaren Übertragungsbadbreite

Da an vielen Orten im Kulturveranstaltungsumfeld nur relativ schlechte
Internetanbindungen verfügbar sind, ist es sinnvoll, die tatsächlich vorhanden
Übertragungskapazitäten für die Einspeisung empirisch zu ermitteln.

Man verwendet dazu bspw. [Iperf3](https://iperf.fr/), das für alle
Betriebssystem sehr einfach erhältlich und umfassende messungen erlaubt.

Am Server ist dazu bspw. folgender Docker-Prozess als Gegegenstelle einzurichten:

    docker run -d -p 5201:5201 --name iperf3-server --restart unless-stopped \
	    registry.gitlab.com/mash-graz/iperf3-server

Clientseitig benutzt man im einfachsten Fall den Befehl: `iperf3 -c servername.mur.at`

Natürlich kann man das selbe Docker-Image auch lokal für den Test heranziehen:

    docker run -ti --rm -p 5201:5201 registry.gitlab.com/mash-graz/iperf3-server \
        iperf3 -c ms.mur.at

Neben diesem einfachen Aufruf stehen in dem Programm aber auch noch einige
weitere nützliche Messmethoden zur Verfügung -- siehe: `iperf3 --help` od.
https://iperf.fr/iperf-doc.php

## Tipps zum Debuggen

Ob die `ffmpeg` worker am Server tatsächlich aktiv sind, sieht man im Falle der
Docker-Nutzung am besten mit folgendem Befehl von außen:

```
docker exec live-video pstree

nginx-+-nginx---ffmpeg---19*[{ffmpeg}]
      `-2*[nginx]
```

Ähnliches gilt für das Überprüfen des HLS-Verzeichnises, wo sich die
Ausliferungsfragmente und Playlists befinden sollten:

```
docker exec live-video ls /tmp/hls

livestream-name.m3u8
livestream-name_low-14.ts
livestream-name_low-15.ts
livestream-name_low-16.ts
livestream-name_low-17.ts
...
```

Und natürlich kann man den stream auch mit einem vernünftigen
Video-Player wiedergeben:

    mpv http://servername.mur.at/hls/livestream-name.m3u8


Sehr hilfreich beim Testen kann es auch sein, wenn man als Zuspielquelle ein
fertig vorliegendes Video-File statt der Kamera verwendet:

    ffmpeg -re -i videofile.mp4 -c:v libx264 -b:v 4M -c:a aac -b:a 160k \
	    -g 2 -f flv rtmp://servername/live/stream-name

Der Server verfügt auch über eine eine eigene Status-Page, die leider allerdings ziemlich buggy wirkt und oft erst nach mehrmaligen Versuchen eine Anzeige liefert.

Man findet sie unter dem URL: `http://servername/stat`

![Status Page](images/stat.png)
