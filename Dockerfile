FROM debian:testing-slim

RUN apt update \
  && apt install -y ffmpeg jed less psmisc ssl-cert curl gnupg \
  nginx-light libnginx-mod-rtmp \
  && rm -rf /var/lib/apt/lists/*

# forward nginx request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log 

COPY html /var/www/html
RUN mkdir /var/www/html/player \
  && cd /var/www/html \
  && curl -LO https://raw.githubusercontent.com/arut/nginx-rtmp-module/master/stat.xsl \
  && cd /var/www/html/player \
  && curl -L https://github.com/matvp91/indigo-player/archive/master.tar.gz \
  | tar --wildcards --strip-components=2 -vzx */lib/*

COPY nginx_config/default /etc/nginx/sites-enabled/default
COPY nginx_config/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 1935

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
